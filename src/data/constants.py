from data.tag import *

"""
WORLD GEN PARAMS
"""

# The number of levels of detail
OCTAVES = 10
# Determines how much each octave contributes to overall shape
PERSISTENCE = 0.5
# Determines how much details is added in each octave
LACUNARITY = 2.0


WATER_LEVEL = 0.475
COAST_LEVEL = 0.5
PLAINS_LEVEL = 0.625
FOREST_LEVEL = 0.8
MOUNTAIN_LEVEL = 1

# Ratio of stone in comparsion to upper grounds
STONE_UNDERWATER_RATIO = 0.6
STONE_COAST_RATIO = 0.7
STONE_PLAINS_RATIO = 0.8
STONE_FOREST_RATIO = 0.9

CIVILISATIONS_NUMBER = 15
POSSIBLE_SPAWNS = [EnvironmentTag.FOREST, EnvironmentTag.PLAINS]

"""
RESOURCE GEN PARAMS
"""

BASIC_SPAWN_CHANCE = 0.001
CHAIN_SPAWN_CHANCE_MOD = 0.07
PLAINS_FOOD_BUFF = 0.07
FOREST_FOOD_BUFF = 0.03
MOUNTAIN_IRON_BUFF = 0.1


"""
CIVILISATION PARAMS
"""


BASE_FOOD_LEVEL = 5
BASE_IRON_LEVEL = 3
