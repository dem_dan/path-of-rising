from typing import *
from .tag import *
import array


class Tile:
    __slots__ = 'tags', 'elevation', 'x', 'y'

    def __init__(self, elevation: int, x: int, y: int, tags: List[Tag] = None):
        if tags is None:
            tags = array.array('H')
        self.elevation = elevation
        self.tags = tags
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return "Tile([{}])".format(", ".join(repr(tag_by_code(code)) for code in self.tags))


def tile_append_tags(tile: Tile, tags: List[Tag]) -> Tile:
    tile.tags.extend(tag.code for tag in tags) if type(tags) is list else tile.tags.append(tags.code)
    return tile


def tile_insert_tag(tile: Tile, pos: int, tag: Tag) -> None:
    tile.tags.insert(pos, tag.code)


def tile_remove_tag(tile: Tile, tag: Tag) -> Tile:
    tile.tags.remove(tag.code)
    return tile


def tile_get_tag_codes(tile: Tile, tag_class) -> List[Tag]:
    tags = filter(lambda x: tag_by_code(x) in tag_class, tile.tags)
    return list(tags)


class TaggingError(Exception):
    pass
