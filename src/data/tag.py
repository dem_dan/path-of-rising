from typing import *
from enum import Enum

_tag_list = []


class Tag(Enum):
    def __init__(self, description: str, code: int):
        self.code = code
        self.description = description
        if code in _tag_list:
            raise TagCodeAlreadyExistsError("Tag code {} already defined".format(code))
        _tag_list.append(code)

    def __new__(cls, *values: list):
        obj = object.__new__(cls)
        obj._value_ = values[0]
        for other_value in values[1:]:
            cls._value2member_map_[other_value] = obj
        obj._all_values = values
        return obj


class TagCodeAlreadyExistsError(Exception):
    pass


class GroundTag(Tag):
    SOIL = "Just regular soil", 101
    DIRT = "Slippery, murky, smelly soil", 102
    SAND = "Sand from a nice beach... Or not so nice desert", 103
    STONE = "Sturdy rock", 104
    WATER = "Just regular water from regular pool", 105
    SALT_WATER = "Salty. Sea water, i guess?", 106
    AIR = "You can feel nice breeze. Oxygen is good", 107
    VOID = "Total, scary emptiness... Why the hell are this thing exists?", 108

    def __init__(self, description: str, code: int):
        super().__init__(description, code)
        self.layer = 0


class EnvironmentTag(Tag):
    FOREST = "A lot of trees", 201
    CITY = "Home for bunch of peoples", 202
    COAST = "Entrance to the kingdom of the sea", 203
    SEA = "Everything is salty and wet. That's the sea", 204
    LAKE = "Enormous cup of drinkable water", 205
    DESERT = "Lifeless hot desert", 206
    PLAINS = "Plains and hills", 207
    MOUNTAIN = "Perky cliffs", 208

    def __init__(self, description: str, code: int):
        super().__init__(description, code)
        self.layer = 1


class ResourceTag(Tag):
    IRON = "Smelt it into the weapons! Improves power of civilisation", 301
    FOOD = "No one can rise with empty stomach. Improves growth of civilisation", 302
    MINERALS = "Some shiny stones, colorful powders. Improves science of civilisation", 303
    GOLD = "True power of this world. Improves economics of civilisation", 304
    MARBLE = "Good for statues. Improves culture of civilisation", 305

    def __init__(self, description: str, code: int):
        super().__init__(description, code)
        self.layer = 2


class StatusTag(Tag):
    ON_FIRE = "Everything burns!", 401
    ARMY = "An army on they way to the glory", 402

    def __init__(self, description: str, code: int):
        super().__init__(description, code)
        self.layer = 3


class PositionalTag(Tag):
    SURFACE = "Nice and comfy (maybe not) ground level", 501
    UNDERGROUND = "Dark and scary depths of the world", 502
    CORNER = "Last piece of matter in this world. Only void awaits ahead", 503
    ABOVE_GROUND = "People cannot fly", 504
    UNDERWATER = "Dark and scary depths of the sea", 505

    def __init__(self, description: str, code: int):
        super().__init__(description, code)
        self.layer = -1

TAGS = {
    100: GroundTag,
    200: EnvironmentTag,
    300: ResourceTag,
    400: StatusTag,
    500: PositionalTag
}

def tag_by_code(code: int) -> Tag:
    subcode = code % 100
    return TAGS[code - subcode](code)

