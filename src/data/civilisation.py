from typing import *
from data.constants import *
from data.tile import *


CIV_COUNTER = 0


class Civilisation:

    def __init__(self, tile: Tile, pos_x: int, pos_y: int):
        global CIV_COUNTER
        CIV_COUNTER += 1
        self.id = CIV_COUNTER
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.resources = {
            ResourceTag.FOOD.code: BASE_FOOD_LEVEL,
            ResourceTag.IRON.code: BASE_IRON_LEVEL,
        }
        self.gathered_tiles = [tile]
        self.power = 0

    def tile_belong_to_civ(self, tile: Tile) -> bool:
        return tile in self.gathered_tiles

    def get_food_production(self) -> int:
        return self._get_production(ResourceTag.FOOD)

    def get_iron_production(self) -> int:
        return self._get_production(ResourceTag.IRON)

    def _get_production(self, tag) -> int:
        prod = 1
        for tile in self.gathered_tiles:
            prod += len(list(filter(lambda x: x == tag.code, tile_get_tag_codes(tile, ResourceTag))))
        return prod

    @staticmethod
    def get_civ_by_tile(world, tile) -> Tuple['Civilisation', bool]:
        for civ in world.civilisations:
            if civ.tile_belong_to_civ(tile):
                return civ, True
        for civ in world.destroyed_civilisations:
            if civ.tile_belong_to_civ(tile):
                return civ, False
        return None, None
