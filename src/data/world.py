from typing import *
import numpy, noise
from .tile import *
from .constants import *
from .civilisation import *
import random


class World:
    def __init__(self, size: Tuple[int, int, int]):
        self.size = size
        self.civilisations = []
        self.destroyed_civilisations = []
        self.map = None

    @staticmethod
    def generate(size: Tuple[int, int, int], scale: int, base: int) -> 'World':
        world = World(size)

        # Generates height map using Perlin noise
        height_map = World._prepare_height_map(size, scale, base)
        world.map = numpy.zeros((size[0], size[1]), dtype=numpy.object)
        # Generates grounds for world using heights as base
        World._generate_grounds_stage(world, size, height_map)
        # Generates resources for world
        World._generate_resources_stage()
        for i in range(size[0]):
            for j in range(size[1]):
                World._spawn_resource(world, i, j, ResourceTag.IRON, replacer=GroundTag.STONE,
                                      modifiers={EnvironmentTag.MOUNTAIN: [MOUNTAIN_IRON_BUFF]},
                                      blacklist_tags=[EnvironmentTag.SEA])
                World._spawn_resource(world, i, j, ResourceTag.FOOD,
                                      modifiers={EnvironmentTag.PLAINS: [PLAINS_FOOD_BUFF],
                                                 EnvironmentTag.FOREST: [FOREST_FOOD_BUFF]},
                                      whitelist_tags=[EnvironmentTag.FOREST, EnvironmentTag.PLAINS])

        # Generates civilisations
        World._spawn_civilisations(world)

        return world

    @staticmethod
    def _prepare_height_map(size: Tuple[int, int, int], scale: int, base: int) -> numpy.ndarray:
        height_map = numpy.zeros((size[0], size[1]), dtype=numpy.float16)

        for i in range(size[0]):
            for j in range(size[1]):
                k = noise.pnoise2(
                    i / scale, j / scale,
                    octaves=OCTAVES,
                    persistence=PERSISTENCE,
                    lacunarity=LACUNARITY,
                    repeatx=size[0],
                    repeaty=size[1],
                    base=base)
                height_map[i][j] = k

        max_height = numpy.max(height_map)
        height_map += max_height
        min_height = numpy.min(height_map)
        height_map += abs(min_height)
        return height_map

    @staticmethod
    def _generate_grounds_stage(world: 'World', size: Tuple[int, int, int], height_map: numpy.ndarray) -> None:
        for i in range(size[0]):
            for j in range(size[1]):

                k = height_map[i][j]
                h = int(k * size[2])

                world.map[i][j] = Tile(h, i, j)

                if k < WATER_LEVEL:
                    World._append_underwater_layer(world.map[i][j], h, int(WATER_LEVEL * size[2]))
                elif k < COAST_LEVEL:
                    World._append_coast_layer(world.map[i][j], h)
                elif k < PLAINS_LEVEL:
                    World._append_plains_layer(world.map[i][j], h)
                elif k < FOREST_LEVEL:
                    World._append_forest_layer(world.map[i][j], h)
                else:
                    World._append_mountain_layer(world.map[i][j], h)

    @staticmethod
    def _generate_resources_stage() -> None:
        pass

    @staticmethod
    def _append_ground(tile: Tile, amount: int, tag: Tag) -> None:
        tile_append_tags(tile, [tag for i in range(amount)])

    @staticmethod
    def _append_underwater_layer(tile: Tile, elevation: int, water_level: int) -> None:
        stone_layer_height = int(elevation * STONE_UNDERWATER_RATIO)

        World._append_ground(tile, water_level - elevation, GroundTag.WATER)
        World._append_ground(tile, elevation - stone_layer_height, GroundTag.SAND)
        World._append_ground(tile, stone_layer_height, GroundTag.STONE)

        tile_append_tags(tile, EnvironmentTag.SEA)

    @staticmethod
    def _append_coast_layer(tile: Tile, elevation: int) -> None:
        stone_layer_height = int(elevation * STONE_COAST_RATIO)

        World._append_ground(tile, elevation - stone_layer_height, GroundTag.SAND)
        World._append_ground(tile, stone_layer_height, GroundTag.STONE)

        tile_append_tags(tile, EnvironmentTag.COAST)

    @staticmethod
    def _append_plains_layer(tile: Tile, elevation: int) -> None:
        stone_layer_height = int(elevation * STONE_PLAINS_RATIO)

        World._append_ground(tile, elevation - stone_layer_height, GroundTag.SOIL)
        World._append_ground(tile, stone_layer_height, GroundTag.STONE)

        tile_append_tags(tile, EnvironmentTag.PLAINS)

    @staticmethod
    def _append_forest_layer(tile: Tile, elevation: int) -> None:
        stone_layer_height = int(elevation * STONE_FOREST_RATIO)

        World._append_ground(tile, elevation - stone_layer_height, GroundTag.SOIL)
        World._append_ground(tile, stone_layer_height, GroundTag.STONE)

        tile_append_tags(tile, EnvironmentTag.FOREST)

    @staticmethod
    def _append_mountain_layer(tile: Tile, elevation: int) -> None:
        World._append_ground(tile, elevation, GroundTag.STONE)

        tile_append_tags(tile, EnvironmentTag.MOUNTAIN)

    @staticmethod
    def _spawn_resource(world: 'World', x: int, y: int, resource_tag: ResourceTag, replacer: Tag = None, modifiers: Dict[Tag, List[int]] = None, whitelist_tags: List[Tag] = None, blacklist_tags: List[Tag] = None, chained: bool = False) -> None:
        tile = world.map[x][y]
        if resource_tag.code in tile_get_tag_codes(tile, ResourceTag):
            return
        if whitelist_tags is not None and not any(tag.code in tile.tags for tag in whitelist_tags):
            return
        if blacklist_tags is not None and any(tag.code in tile.tags for tag in blacklist_tags):
            return

        World._init_spawn(world, x, y, resource_tag, replacer, modifiers, whitelist_tags, blacklist_tags, chained)

    @staticmethod
    def _init_spawn(world: 'World', x: int, y: int, resource_tag: ResourceTag, replacer: Tag, modifiers: Dict[Tag, List[int]], whitelist_tags: List[Tag], blacklist_tags: List[Tag], chained: bool) -> None:
        tile = world.map[x][y]
        result_chance = BASIC_SPAWN_CHANCE
        if modifiers is not None:
            for tag, mod in modifiers.items():
                if tag.code in tile.tags:
                    for m in mod:
                        result_chance += m

        rng = random.uniform(0, 1)

        if rng < result_chance:
            replacements = None
            if replacer is not None:
                replacements = [i for i, e in enumerate(tile.tags) if e == replacer.code]

            if replacements is None:
                tile_insert_tag(tile, 0, resource_tag)
            else:
                amount = round(random.random() * (len(replacements) // 2)) + 1
                for i in range(0, amount):
                    idx = random.choice(replacements)
                    replacements.remove(idx)
                    tile.tags[idx] = resource_tag.code

    @staticmethod
    def _spawn_civilisations(world: 'World') -> None:
        max_x = world.size[0] - 1
        max_y = world.size[1] - 1
        while len(world.civilisations) < CIVILISATIONS_NUMBER:
            rnd_x = round(random.random() * max_x)
            rnd_y = round(random.random() * max_y)
            tile = world.map[rnd_x][rnd_y]
            env_tags = tile_get_tag_codes(tile, EnvironmentTag)
            if any(tag.code in env_tags for tag in POSSIBLE_SPAWNS):
                tile_insert_tag(tile, 0, EnvironmentTag.CITY)
                world.civilisations.append(Civilisation(tile, rnd_x, rnd_y))
