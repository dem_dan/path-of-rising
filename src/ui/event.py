from typing import *
import pygame as pg

MOUSE_EVENT_TYPES = (pg.MOUSEMOTION, pg.MOUSEWHEEL, pg.MOUSEBUTTONDOWN, pg.MOUSEBUTTONUP)
KEYBOARD_EVENT_TYPES = (pg.KEYDOWN, pg.KEYUP)
CUSTOM_HOVER_EVENT_TYPE = pg.USEREVENT + 1
CUSTOM_DRAG_EVENT_TYPE = pg.USEREVENT + 2


class EventListener:
    def __init__(self):
        self._events = {}

    def on(self, event_type, action) -> None:
        self._events[event_type] = action

    def restore(self) -> None:
        pass


class EventManager:
    def __init__(self):
        self._mouse_events = None
        self._keyboard_events = None
        self._quit_event = None
        self._listeners = {CUSTOM_HOVER_EVENT_TYPE: [], CUSTOM_DRAG_EVENT_TYPE: []}

    def get_mouse_events(self) -> List[pg.event.Event]:
        return self._mouse_events

    def get_keyboard_events(self) -> List[pg.event.Event]:
        return self._keyboard_events

    def get_quit_event(self) -> pg.event.Event:
        return self._quit_event

    def get_mouse_pos(self) -> Tuple[int, int]:
        return pg.mouse.get_pos()

    def get_mouse_rel(self) -> Tuple[int, int]:
        return pg.mouse.get_rel()

    def add_listener(self, event_type: int, listener: EventListener, action: Callable) -> None:
        if event_type not in self._listeners:
            self._listeners[event_type] = list()
        self._listeners[event_type].append(listener)
        listener.on(event_type, action)

    def update_events(self) -> None:
        events = pg.event.get()
        self._mouse_events = filter(lambda event: event.type in MOUSE_EVENT_TYPES, events)
        self._keyboard_events = filter(lambda event: event.type in KEYBOARD_EVENT_TYPES, events)
        self._quit_event = next(filter(lambda event: event.type == pg.QUIT, events), None)
        pg.event.clear()

    def notify_mouse_listeners(self) -> None:
        if pg.mouse.get_focused():
            for listener in self._listeners[CUSTOM_HOVER_EVENT_TYPE]:
                if listener.hover(self.get_mouse_pos()):
                    listener.handle_mouse(
                        pg.event.Event(CUSTOM_HOVER_EVENT_TYPE, pos=self.get_mouse_pos(), rel=self.get_mouse_rel()))
                    continue
                else:
                    listener.restore()
            for event in self._mouse_events:
                for listener in self._listeners[CUSTOM_DRAG_EVENT_TYPE]:
                    if event.type == pg.MOUSEMOTION and event.buttons[0] == 1 and listener.hover(self.get_mouse_pos()):
                        listener.handle_mouse(
                            pg.event.Event(CUSTOM_DRAG_EVENT_TYPE, pos=event.pos, rel=event.rel))
                        continue
                if event.type in self._listeners:
                    for listener in self._listeners[event.type]:
                        if listener.hover(self.get_mouse_pos()):
                            listener.handle_mouse(event)

    def notify_keyboard_listeners(self) -> None:
        if pg.mouse.get_focused():
            for event in self._keyboard_events:
                if event.type in self._listeners:
                    for listener in self._listeners[event.type]:
                        listener.handle_keyboard(event)


class MouseEventListener(EventListener):
    def hover(self, mouse_pos: Tuple[int, int]) -> bool:
        pass

    def handle_mouse(self, event: pg.event.Event = None) -> None:
        if event.type == CUSTOM_HOVER_EVENT_TYPE:
            self._on_mouse_hover(event)
        elif event.type == CUSTOM_DRAG_EVENT_TYPE:
            self._on_mouse_drag(event)
        elif event.type == pg.MOUSEMOTION:
            self._on_mouse_motion(event)
        elif event.type == pg.MOUSEWHEEL:
            self._on_mouse_wheel(event)
        elif event.type == pg.MOUSEBUTTONDOWN:
            self._on_mouse_button_down(event)
        elif event.type == pg.MOUSEBUTTONUP:
            self._on_mouse_button_up(event)

    def _on_mouse_motion(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_wheel(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_button_down(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_button_up(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_hover(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_drag(self, event: pg.event.Event = None) -> None:
        pass


class KeyboardEventListener(EventListener):
    def handle_keyboard(self, event: pg.event.Event = None) -> None:
        if event.type == pg.K_DOWN:
            self._on_key_down(event)
        elif event.type == pg.K_UP:
            self._on_key_up(event)

    def _on_key_down(self, event: pg.event.Event = None) -> None:
        pass

    def _on_key_up(self, event: pg.event.Event = None) -> None:
        pass
