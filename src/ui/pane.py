from typing import *
import pygame as pg
from .widget import Widget


class GridPane(Widget):
    def __init__(self, rows: int, cols: int, padding: int = 0, **kwargs):
        super().__init__(**kwargs)
        self._rows = rows
        self._cols = cols
        self._padding = padding
        self._children = {}
        self._children_meta = {}
        self._cell_size = None
        self.recompute()

    def recompute(self) -> None:
        self.compute_cell_size()

    def compute_cell_size(self) -> None:
        self._cell_size = self.width / self._cols - self._padding * 2, self.height / self._rows - self._padding * 2

    def add_child(self, child: Widget, pos: Tuple[int, int], h_span: int = 1, v_span: int = 1) -> None:
        col_0, row_0 = pos
        col_1, row_1 = pos
        child.left = self._cell_size[0] * col_0 + (self._padding * 2 * col_0) + self._padding
        child.top = self._cell_size[1] * row_0 + (self._padding * 2 * row_0) + self._padding
        child.width = self._cell_size[0]
        child.height = self._cell_size[1]
        if h_span > 1:
            col_1 = col_0 + h_span - 1
            num_cols = col_1 - col_0
            child.width = self._cell_size[0] * (num_cols + 1) + self._padding * num_cols * 2
        if v_span > 1:
            row_1 = row_0 + v_span - 1
            num_rows = row_1 - row_0
            child.height = self._cell_size[1] * (num_rows + 1) + self._padding * num_rows * 2
        self._children[pos] = child
        self._children_meta[pos] = (h_span, v_span)
        child.set_parent(self)

    def render(self, surface: pg.Surface = None) -> None:
        super().render(surface)
        for child in self._children.values():
            child.render(self._surface)
