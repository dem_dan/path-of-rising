from typing import *
import pygame as pg
from .widget import Widget


class Label(Widget):
    def __init__(self, text: str, font: pg.font.SysFont, h_center: bool = True, v_center: bool = True, padding: Tuple[int, int] = (0, 0), **kwargs):
        super().__init__(**kwargs)
        self._text = text
        self._font = font
        self._h_center = h_center
        self._v_center = v_center
        self._padding = padding
        self._r_text = None
        self._text_size = None
        self._update_r_text()

    def _update_r_text(self) -> None:
        self._text_size = self._font.size(self._text)
        self._r_text = self._font.render(self._text, True, self._color)

    def get_font(self) -> pg.font.SysFont:
        return self._font

    def get_text(self) -> str:
        return self._text

    def set_font(self, font: pg.font.SysFont) -> None:
        self._font = font
        self._update_r_text()

    def set_text(self, text: str) -> None:
        self._text = text
        self._update_r_text()

    def render(self, surface: pg.Surface = None) -> None:
        super().render(surface)
        if not self._h_center and not self._v_center:
            words = [word.split(' ') for word in self._text.splitlines()]
            space = self._font.size(' ')[0]
            max_width, max_height = surface.get_size()
            x, y = self.left, self.top
            for line in words:
                for word in line:
                    word_surface = self._font.render(word, True, self._color)
                    word_width, word_height = word_surface.get_size()
                    if x + word_width >= max_width:
                        x = self.left
                        y += word_height
                    surface.blit(word_surface, (x, y))
                    x += word_width + space
                x = self.left
                y += word_height
        pos = ((self.width / 2 - self._text_size[0] / 2) if self._h_center else self._padding[0],
               (self.height / 2 - self._text_size[1] / 2 + 2) if self._v_center else self._padding[1])
        self._surface.blit(self._r_text, pos)
