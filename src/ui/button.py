from typing import *
import pygame as pg
from .label import Label
from .event import MouseEventListener, CUSTOM_HOVER_EVENT_TYPE, CUSTOM_DRAG_EVENT_TYPE


class Button(Label, MouseEventListener):
    DEFAULT_SIZE = (100, 30)
    DEFAULT_BACKGROUND_COLOR = (0, 105, 92)
    DEFAULT_HOVER_BACKGROUND_COLOR = (0, 137, 123)

    def __init__(self, text: str, font: pg.font.SysFont,
                 bg_normal_color: Tuple[int, int, int] = DEFAULT_BACKGROUND_COLOR,
                 normal_color: Tuple[int, int, int] = Label.DEFAULT_COLOR,
                 bg_hover_color: Tuple[int, int, int] = DEFAULT_HOVER_BACKGROUND_COLOR,
                 hover_color: Tuple[int, int, int] = Label.DEFAULT_COLOR, **kwargs):
        super().__init__(text, font, bg_color=bg_normal_color, **kwargs)
        self._bg_normal_color = bg_normal_color
        self._normal_color = normal_color
        self._bg_hover_color = bg_hover_color
        self._hover_color = hover_color
        self._events = {}

    def _on_mouse_button_down(self, event: pg.event.Event = None) -> None:
        self._events[event.type](self)

    def _on_mouse_hover(self, event: pg.event.Event = None) -> None:
        self._bg_color = self._bg_hover_color
        self._events[event.type](self)

    def _on_mouse_drag(self, event: pg.event.Event = None) -> None:
        pass

    def hover(self, mouse_pos: Tuple[int, int]) -> bool:
        abs_rect = self.copy()
        abs_rect.left, abs_rect.top = self.get_abs_pos()
        return abs_rect.collidepoint(mouse_pos)

    def restore(self) -> None:
        self._bg_color = self._bg_normal_color
