from typing import *
import pygame as pg


class Widget(pg.Rect):
    DEFAULT_BACKGROUND_COLOR = (84, 110, 122)
    DEFAULT_COLOR = (236, 239, 241)

    def __init__(self, pos: Tuple[int, int] = None, size: int = None, parent: 'Widget' = None, surface: pg.Surface = None, bg_color: Tuple[int, int, int] = DEFAULT_BACKGROUND_COLOR, color: Tuple[int, int, int] = DEFAULT_COLOR):
        if pos is None:
            pos = (0, 0)
        if size is None:
            size = (0, 0)
        super().__init__(*pos, *size)
        self._parent = parent
        self._surface = surface
        self._bg_color = bg_color
        self._color = color

    def get_parent(self) -> 'Widget':
        return self._parent

    def get_surface(self) -> pg.Surface:
        return self._surface

    def get_abs_pos(self) -> Tuple[int, int]:
        if self._parent is not None:
            parent_pos = self._parent.get_abs_pos()
            return self.left + parent_pos[0], self.top + parent_pos[1]
        return self.left, self.top

    def set_parent(self, parent: 'Widget') -> None:
        self._parent = parent

    def set_surface(self, surface: pg.Surface) -> None:
        self._surface = surface

    def inherit_surface(self, surface: pg.Surface = None) -> None:
        if surface is None and self._surface is None:
            raise RuntimeError('No surface to draw on')
        elif surface is not None and self._surface is None:
            self._surface = surface.subsurface(self)

    def update_surface(self, surface: pg.Surface = None) -> None:
        if surface is not None:
            del self._surface
            self._surface = surface.subsurface(self)
        else:
            self._surface = self._surface.get_parent().subsurface(self)

    def render(self, surface: pg.Surface = None) -> None:
        self.inherit_surface(surface)
        pg.draw.rect(self._surface, self._bg_color, (0, 0, self.width, self.height))
