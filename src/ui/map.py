from typing import *
import pygame as pg
from .widget import Widget
from .event import MouseEventListener, KeyboardEventListener, CUSTOM_DRAG_EVENT_TYPE
from data.tag import EnvironmentTag, ResourceTag, GroundTag
from data.tile import tile_get_tag_codes
from data.constants import CIVILISATIONS_NUMBER
from data.world import World
from main import Color


class Camera:
    def __init__(self, x: int, y: int, zoom: int):
        self.x = x
        self.y = y
        self.zoom = zoom

    def inc_zoom(self, delta: int) -> None:
        if self.zoom + delta >= 1:
            self.zoom += delta
        else:
            self.zoom = 1


class MatrixView:
    def __init__(self, start: Tuple[int, int], end: Tuple[int, int]):
        self.start = start
        self.end = end

    def get_length(self) -> int:
        return self.end[0] - self.start[0]


class Map(Widget, MouseEventListener, KeyboardEventListener):
    def __init__(self, data: World, cam_zoom: int = 1, **kwargs):
        super().__init__(**kwargs)
        self._data = data
        self._cam = Camera(0, 0, cam_zoom)
        self._view = MatrixView((0, 0), (data.map.shape[0] - 1, data.map.shape[1] - 1))
        self._cursor_pos = (0, 0)
        self._tile_size = None
        self._offset = None
        self._events = {}
        self._render_required = True
        self._image = None
        self.recompute()

    def _on_mouse_wheel(self, event: pg.event.Event = None) -> None:
        self.inc_zoom(event.y)

    def _on_mouse_drag(self, event: pg.event.Event = None) -> None:
        self.compute_cam(event.rel)
        self.compute_view()

    def _on_mouse_motion(self, event: pg.event.Event = None) -> None:
        pass

    def _on_mouse_button_down(self, event: pg.event.Event = None) -> None:
        if event.button == 1:
            cur_pos = (event.pos[0] - self._offset[0]) // self._tile_size + self._cam.x, \
                      (event.pos[1] - self._offset[1]) // self._tile_size + self._cam.y
            if 0 <= cur_pos[0] < self._view.end[0] and 0 <= cur_pos[1] < self._view.end[1]:
                self._cursor_pos = cur_pos
            self.click(event.type)

    def _on_key_down(self, event: pg.event.Event = None) -> None:
        self._render_required = True
        if event.key == pg.K_UP:
            self.compute_cam((0, 1))
        elif event.key == pg.K_LEFT:
            self.compute_cam((1, 0))
        elif event.key == pg.K_DOWN:
            self.compute_cam((0, -1))
        elif event.key == pg.K_RIGHT:
            self.compute_cam((-1, 0))
        self.compute_view()

    def require_update(self) -> None:
        self._render_required = True

    def recompute(self) -> None:
        self._render_required = True
        self.compute_tile_size()
        self.compute_offset()

    def click(self, event_type: int) -> None:
        self._events[event_type](self, self._data.map[int(self._cursor_pos[0])][int(self._cursor_pos[1])])

    def hover(self, mouse_pos: Tuple[int, int]) -> bool:
        abs_rect = self.copy()
        abs_rect.left, abs_rect.top = self.get_abs_pos()
        return abs_rect.collidepoint(mouse_pos)

    def inc_zoom(self, delta: int) -> None:
        self._cam.inc_zoom(delta)
        self.compute_cam((0, 0))
        self.compute_view()
        self.recompute()

    def compute_tile_size(self) -> None:
        self._tile_size = min(self.width, self.height) / self._view.get_length()

    def compute_offset(self) -> None:
        tiles_width = self._tile_size * self._view.get_length()
        self._offset = (self.width - tiles_width) / 2, (self.height - tiles_width) / 2

    def compute_view(self) -> None:
        view_length = self._data.map.shape[0] / self._cam.zoom
        self._view.start = int(self._cam.x), int(self._cam.y)
        self._view.end = int(self._cam.x + view_length), int(self._cam.y + view_length)

    def compute_cam(self, delta: int) -> None:
        self._render_required = True
        self._cam.x, self._cam.y = self._cam.x - delta[0], \
                                   self._cam.y - delta[1]
        self._cam.x = self._cam.x if self._cam.x >= 0 else 0
        self._cam.y = self._cam.y if self._cam.y >= 0 else 0
        view_length = self._data.map.shape[0] // self._cam.zoom
        self._cam.x = self._cam.x if self._cam.x + view_length < self._data.map.shape[0] else self._data.map.shape[0] - view_length
        self._cam.y = self._cam.y if self._cam.y + view_length < self._data.map.shape[1] else self._data.map.shape[0] - view_length

    def render(self, surface: pg.Surface = None) -> None:
        if self._render_required:
            super().render(surface)
            self.update_surface(surface)
            for i in range(self._view.start[0], self._view.end[0]):
                for j in range(self._view.start[1], self._view.end[1]):
                    if self._data.map[i][j].tags[0] == EnvironmentTag.CITY.code:
                        color = [255, 0, 255]
                    elif ResourceTag.IRON.code in tile_get_tag_codes(self._data.map[i][j], ResourceTag):
                        color = [255, 0, 0]
                    elif ResourceTag.FOOD.code in tile_get_tag_codes(self._data.map[i][j], ResourceTag):
                        color = [255, 255, 0]
                    elif tile_get_tag_codes(self._data.map[i][j], GroundTag)[0] == GroundTag.WATER.code:
                        color = Color.WATER
                    elif tile_get_tag_codes(self._data.map[i][j], GroundTag)[0] == GroundTag.SAND.code:
                        color = Color.BEACH
                    elif tile_get_tag_codes(self._data.map[i][j], GroundTag)[0] == GroundTag.SOIL.code:
                        color = Color.PLAIN
                    elif tile_get_tag_codes(self._data.map[i][j], GroundTag)[0] == GroundTag.STONE.code:
                        color = Color.MOUNTAIN
                    rect = (self._offset[0] + (i - self._view.start[0]) * self._tile_size,
                            self._offset[1] + (j - self._view.start[1]) * self._tile_size,
                            self._tile_size, self._tile_size)
                    for civil in self._data.civilisations:
                        if civil.tile_belong_to_civ(self._data.map[i][j]):
                            col_id = 255 // CIVILISATIONS_NUMBER * civil.id
                            color = (col_id, col_id, col_id)
                    pg.draw.rect(self._surface, color, rect)
            self._image = self._surface.copy()
            self.click(pg.MOUSEBUTTONDOWN)
        else:
            surface.blit(self._image, self)
        cur_pos = self._offset[0] + self._cursor_pos[0] * self._tile_size, \
                  self._offset[1] + self._cursor_pos[1] * self._tile_size
        pg.draw.lines(self._surface, self._bg_color, True, (
            (cur_pos[0], cur_pos[1]), (cur_pos[0] + self._tile_size, cur_pos[1]),
            (cur_pos[0] + self._tile_size, cur_pos[1] + self._tile_size),
            (cur_pos[0], cur_pos[1] + self._tile_size)
        ), self._cam.zoom)
        self._render_required = False
