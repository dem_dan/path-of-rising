from typing import *
from .gen_settings import *
from data.world import *
import random


TURNS = 0


def turn(world: World) -> int:
    global TURNS
    print("Epoch {}".format(TURNS))
    TURNS += 1
    for civ in world.civilisations:
        civ.resources[ResourceTag.FOOD.code] += civ.get_food_production()
        civ.resources[ResourceTag.IRON.code] += civ.get_iron_production()
        _make_decision(world, civ)
    return TURNS


def _make_decision(world: World, civilisation: Civilisation) -> None:
    prey_raid, iron_cons_raid, food_cons_raid = _check_and_get_if_can_attack_someone(civilisation, world, IRON_CONSUMPTION_FOR_RAIDING_MULTIPLIER)
    prey_dest, iron_cons_dest, food_cons_dest = _check_and_get_if_can_attack_someone(civilisation, world, IRON_CONSUMPTION_FOR_DESTROYING_MULTIPLIER)
    obtain_check = _check_can_obtain_title(civilisation)
    produce_check = _check_can_produce_power(civilisation)

    choices = ['food', 'iron', 'power', 'raid', 'elimination']

    food_obtaining_chance = 0
    iron_obtaining_chance = 0
    power_chance = 0
    raiding_chance = 0
    eliminating_chance = 0

    if obtain_check:
        food_obtaining_chance += (1 / civilisation.get_food_production()) * FOOD_IMPORTANCE
    if obtain_check:
        iron_obtaining_chance += (1 / civilisation.get_iron_production()) * IRON_IMPORTANCE
    if produce_check:
        power_chance += (1 / civilisation.get_iron_production()) * POWER_IMPORTANCE
    if prey_raid is not None:
        raiding_chance += ((1 / civilisation.get_food_production()) + (1 / civilisation.get_iron_production())) * WAR_IMPORTANCE
    if prey_dest is not None:
        eliminating_chance += ((1 / civilisation.get_food_production()) * (1 / civilisation.get_iron_production())) * WAR_IMPORTANCE

    total = food_obtaining_chance + iron_obtaining_chance + raiding_chance + eliminating_chance + power_chance

    weights = [
        0 if food_obtaining_chance == 0 else round(food_obtaining_chance / total * 100),
        0 if iron_obtaining_chance == 0 else round(iron_obtaining_chance / total * 100),
        0 if power_chance == 0 else round(power_chance / total * 100),
        0 if raiding_chance == 0 else round(raiding_chance / total * 100),
        0 if eliminating_chance == 0 else round(eliminating_chance / total * 100),
    ]

    if all(weight == 0 for weight in weights):
        print("Civilisation with id {} skipping turn and collecting resources".format(civilisation.id))
        return

    rnd = random.choices(choices, weights)

    if 'food' in rnd:
        print("Civilisation with id {} chosen to extend their territory in search for FOOD".format(civilisation.id))
        _take_tile(world, civilisation, ResourceTag.FOOD)
    elif 'iron' in rnd:
        print("Civilisation with id {} chosen to extend their territory in search for IRON".format(civilisation.id))
        _take_tile(world, civilisation, ResourceTag.IRON)
    elif 'power' in rnd:
        print("Civilisation with id {} chosen to raise their power with iron".format(civilisation.id))
        _produce_power(civilisation)
    elif 'raid' in rnd:
        print("Civilisation with id {} chosen to raid on civilisation {}".format(civilisation.id, prey_raid.id))
        _attack_other_civilisation(civilisation, prey_raid, food_cons_raid, world)
    elif 'elimination' in rnd:
        print("Civilisation with id {} chosen to take down civilisation {}".format(civilisation.id, prey_dest.id))
        _destroy_other_civilisation(civilisation, prey_dest, food_cons_dest, world)


def _attack_other_civilisation(aggressor: Civilisation, prey: Civilisation, food_consumption: float, world: World) -> None:
    _perform_attack_resource_swap(aggressor, prey, food_consumption, RAID_PROFIT, IRON_CONSUMPTION_FOR_RAIDING_MULTIPLIER)

    prey.power -= RAID_DAMAGE
    if prey.power < 0:
        world.civilisations.remove(prey)
        world.destroyed_civilisations.append(prey)
        print("Civilisation {} was abandoned after attack of {}".format(prey.id, aggressor.id))


def _destroy_other_civilisation(aggressor: Civilisation, prey: Civilisation, food_consumption: float, world: World) -> None:
    _perform_attack_resource_swap(aggressor, prey, food_consumption, DESTRUCTION_PROFIT, IRON_CONSUMPTION_FOR_DESTROYING_MULTIPLIER)
    world.civilisations.remove(prey)
    world.destroyed_civilisations.append(prey)
    print("Civilisation {} was destroyed by {} to ruins".format(prey.id, aggressor.id))


def _perform_attack_resource_swap(aggressor: Civilisation, prey: Civilisation, food_consumption: float, gathering_mod: float, iron_cons_mod: float) -> None:
    aggressor.resources[ResourceTag.IRON.code] -= round(prey.power * iron_cons_mod)
    aggressor.resources[ResourceTag.FOOD.code] -= round(food_consumption)
    taken_iron = prey.resources[ResourceTag.IRON.code] * round(gathering_mod)
    taken_food = prey.resources[ResourceTag.FOOD.code] * round(gathering_mod)
    prey.resources[ResourceTag.IRON.code] -= round(taken_iron)
    prey.resources[ResourceTag.FOOD.code] -= round(taken_food)
    aggressor.resources[ResourceTag.IRON.code] += round(taken_iron)
    aggressor.resources[ResourceTag.FOOD.code] += round(taken_food)


def _check_and_get_if_can_attack_someone(civilisation: Civilisation, world: World, iron_con_mod: float) -> Tuple[Civilisation, float, float]:
    civilisations_copy = world.civilisations.copy()
    civilisations_copy.remove(civilisation)
    power_map = {civ: civ.power for civ in civilisations_copy}
    distance_map = {civ: abs(civ.pos_x - civilisation.pos_x) + abs(civ.pos_y - civilisation.pos_y) for civ in civilisations_copy}
    sorted_by_power = dict(sorted(power_map.items(), key=lambda item: item[1]))

    for prey, power in sorted_by_power.items():
        iron_consumption = power * iron_con_mod
        food_consumption = distance_map.get(prey) * FOOD_CONSUMPTION_BY_DISTANCE_MULTIPLIER

        have_iron = iron_consumption < civilisation.resources[ResourceTag.IRON.code]
        have_food = food_consumption < civilisation.resources[ResourceTag.FOOD.code]
        have_revenue = _calculate_raid_profit(prey, iron_consumption, food_consumption)

        if have_iron and have_food and have_revenue:
            return prey, iron_consumption, food_consumption

    return None, None, None


def _calculate_raid_profit(civilisation: Civilisation, iron_consumption: float, food_consumption: float) -> bool:
    return civilisation.resources[ResourceTag.IRON.code] - iron_consumption + civilisation.resources[ResourceTag.FOOD.code] - food_consumption > 0


def _check_can_produce_power(civilisation: Civilisation) -> bool:
    return civilisation.resources[ResourceTag.IRON.code] > IRON_CONSUME_FOR_POWER


def _produce_power(civilisation: Civilisation) -> None:
    civilisation.resources[ResourceTag.IRON.code] -= IRON_CONSUME_FOR_POWER
    civilisation.power += POWER_GATHERING_FROM_IRON


def _check_can_obtain_title(civilisation: Civilisation) -> bool:
    return civilisation.resources[ResourceTag.FOOD.code] > len(civilisation.gathered_tiles)


def _take_tile(world: World, civilisation: Civilisation, preference_resource: ResourceTag) -> None:
    border_tiles = set()
    _fill_borders(civilisation, world.map[civilisation.pos_x][civilisation.pos_y], world, border_tiles)
    resource_map = {tile: len(list(filter(lambda x: x == preference_resource.code, tile_get_tag_codes(tile, ResourceTag)))) for tile in border_tiles}
    sorted_resource_map = dict(sorted(resource_map.items(), key=lambda item: item[1], reverse=True))

    steps = 0
    extended_border = border_tiles.copy()
    while list(sorted_resource_map.values())[0] == 0:
        steps += 1
        extended_border = _step_search_for_resource(civilisation, world, extended_border)
        resource_map = {
            tile: len(list(filter(lambda x: x == preference_resource.code, tile_get_tag_codes(tile, ResourceTag)))) for
            tile in extended_border}
        sorted_resource_map = dict(sorted(resource_map.items(), key=lambda item: item[1], reverse=True))

    target_tile = list(sorted_resource_map.keys())[0]
    if steps == 0:
        civilisation.gathered_tiles.append(target_tile)
        civilisation.resources[ResourceTag.FOOD.code] -= len(civilisation.gathered_tiles)
        print("Civilisation {} taken targeted resource {}".format(civilisation.id, preference_resource))
    else:
        distances = {tile: abs(target_tile.x - tile.x) + abs(target_tile.y - tile.y) for tile in border_tiles}
        sorted_distances = dict(sorted(distances.items(), key=lambda item: item[1]))
        civilisation.gathered_tiles.append(list(sorted_distances.keys())[0])
        civilisation.resources[ResourceTag.FOOD.code] -= len(civilisation.gathered_tiles)


def _fill_borders(civilisation: Civilisation, tile: Tile, world: World, border_tiles: Set[Tile]) -> None:
    if civilisation.tile_belong_to_civ(tile):
        if civilisation.pos_y <= tile.y < world.size[1] - 1:
            _fill_borders(civilisation, world.map[tile.x][tile.y + 1], world, border_tiles)
        if civilisation.pos_y >= tile.y > 0:
            _fill_borders(civilisation, world.map[tile.x][tile.y - 1], world, border_tiles)

        if civilisation.pos_x <= tile.x < world.size[0] - 1:
            _fill_borders(civilisation, world.map[tile.x + 1][tile.y], world, border_tiles)
        if civilisation.pos_x >= tile.x > 0:
            _fill_borders(civilisation, world.map[tile.x - 1][tile.y], world, border_tiles)

    else:
        if not _tile_belongs_to_someone(world, tile):
            border_tiles.add(tile)


def _step_search_for_resource(civilisation: Civilisation, world: World, border_search: Set[Tile]) -> Set[Tile]:
    extended_border = set()
    for tile in border_search:
        if civilisation.pos_y <= tile.y < world.size[1] - 1:
            if not _tile_belongs_to_someone(world, world.map[tile.x][tile.y + 1]):
                extended_border.add(world.map[tile.x][tile.y + 1])
        if civilisation.pos_y >= tile.y > 0:
            if not _tile_belongs_to_someone(world, world.map[tile.x][tile.y - 1]):
                extended_border.add(world.map[tile.x][tile.y - 1])

        if civilisation.pos_x <= tile.x < world.size[0] - 1:
            if not _tile_belongs_to_someone(world, world.map[tile.x + 1][tile.y]):
                extended_border.add(world.map[tile.x + 1][tile.y])
        if civilisation.pos_x >= tile.x > 0:
            if not _tile_belongs_to_someone(world, world.map[tile.x - 1][tile.y]):
                extended_border.add(world.map[tile.x - 1][tile.y])

    return extended_border


def _tile_belongs_to_someone(world: World, tile: Tile) -> bool:
    return any(civ.tile_belong_to_civ(tile) for civ in world.civilisations)
