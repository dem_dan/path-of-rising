import pygame as pg
import numpy as np
from ui.event import *
from ui.pane import *
from ui.map import *
from ui.button import *
from data.world import World
from data.civilisation import Civilisation
from data.tag import tag_by_code
from data.tile import tile_get_tag_codes, GroundTag, ResourceTag, EnvironmentTag
from epoch.gen_machine import turn
from collections import Counter


def dummy_func(*args):
    pass


class Color:
    WATER = [65, 105, 225]
    BEACH = [238, 214, 175]
    PLAIN = [34, 139, 34]
    MOUNTAIN = [139, 137, 137]


def get_world():
    return World.generate((80, 80, 32), 50, 5)


def colorize(world):
    shape = world.map.shape
    colors = np.zeros((shape[0], shape[1], 3), dtype=np.uint8)

    for i in range(shape[0]):
        for j in range(shape[1]):
            if world.map[i][j].tags[0] == EnvironmentTag.CITY.code:
                colors[i][j] = [255, 0, 255]
            elif ResourceTag.IRON.code in tile_get_tag_codes(world.map[i][j], ResourceTag):
                colors[i][j] = [255, 0, 0]
            elif ResourceTag.FOOD.code in tile_get_tag_codes(world.map[i][j], ResourceTag):
                colors[i][j] = [255, 255, 0]
            elif tile_get_tag_codes(world.map[i][j], GroundTag)[0] == GroundTag.WATER.code:
                colors[i][j] = Color.WATER
            elif tile_get_tag_codes(world.map[i][j], GroundTag)[0] == GroundTag.SAND.code:
                colors[i][j] = Color.BEACH
            elif tile_get_tag_codes(world.map[i][j], GroundTag)[0] == GroundTag.SOIL.code:
                colors[i][j] = Color.PLAIN
            elif tile_get_tag_codes(world.map[i][j], GroundTag)[0] == GroundTag.STONE.code:
                colors[i][j] = Color.MOUNTAIN
    return colors


if __name__ == '__main__':
    pg.init()
    world = get_world()
    font = pg.font.SysFont(None, 24)
    font_1 = pg.font.SysFont(None, 20)
    clock = pg.time.Clock()
    win_size = (1200, 936)
    surface = pg.display.set_mode(win_size, flags = pg.HWSURFACE | pg.DOUBLEBUF)

    event_manager = EventManager()

    pane_0 = GridPane(5, 5, 5, size=win_size)
    pane_1 = GridPane(15, 3, 5, bg_color=(144, 164, 174))
    pane_2 = GridPane(3, 3, 0)

    map_0 = Map(world, cam_zoom=4)

    pane_0.add_child(map_0, (0, 0), h_span=4, v_span=5)
    pane_0.add_child(pane_1, (4, 0), v_span=5)

    map_0.compute_cam((0, 0))
    map_0.compute_view()
    map_0.recompute()
    pane_1.recompute()

    label_1 = Label('', font_1, h_center=False, v_center=False)
    label_2 = Label('', font_1, h_center=False, v_center=False)

    button_0 = Button('Zoom In', font)
    button_1 = Button('Zoom Out', font)
    button_2 = Button('Next Turn', font)
    button_3 = Button('Up', font)
    button_4 = Button('Left', font)
    button_5 = Button('Bottom', font)
    button_6 = Button('Right', font)

    pane_1.add_child(button_0, (0, 0), h_span=3)
    pane_1.add_child(button_1, (0, 1), h_span=3)
    pane_1.add_child(button_2, (0, 3), h_span=3)
    pane_1.add_child(label_1, (0, 4), h_span=3, v_span=4)
    pane_1.add_child(label_2, (0, 8), h_span=3, v_span=4)
    pane_1.add_child(pane_2, (0, 12), h_span=3, v_span=3)

    pane_2.recompute()

    pane_2.add_child(button_3, (1, 0))
    pane_2.add_child(button_4, (0, 1))
    pane_2.add_child(button_5, (1, 2))
    pane_2.add_child(button_6, (2, 1))

    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_0, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_1, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_2, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_3, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_4, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_5, dummy_func)
    event_manager.add_listener(CUSTOM_HOVER_EVENT_TYPE, button_6, dummy_func)
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_0, lambda _: map_0.inc_zoom(1))
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_1, lambda _: map_0.inc_zoom(-1))
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_2, lambda self: [self.set_text('Next Turn ({})'.format(turn(world))), map_0.require_update()])
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_3,
                               lambda _: [map_0.compute_cam((0, 1)), map_0.compute_view()])
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_4,
                               lambda _: [map_0.compute_cam((1, 0)), map_0.compute_view()])
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_5,
                               lambda _: [map_0.compute_cam((0, -1)), map_0.compute_view()])
    event_manager.add_listener(pg.MOUSEBUTTONDOWN, button_6,
                               lambda _: [map_0.compute_cam((-1, 0)), map_0.compute_view()])
    event_manager.add_listener(pg.MOUSEWHEEL, map_0, dummy_func)
    event_manager.add_listener(pg.MOUSEMOTION, map_0, dummy_func)
    event_manager.add_listener(CUSTOM_DRAG_EVENT_TYPE, map_0, dummy_func)

    def show_tile_info(tile):
        label_1.set_text(
            ', '.join('%s: %d' % (k, v) for k, v in Counter((tag_by_code(x).name for x in tile.tags)).items()))

    def show_civ_info(tile):
        civ, alive = Civilisation.get_civ_by_tile(world, tile)
        if civ:
            if alive:
                pairs = [('ID', civ.id), ('POWER', civ.power), ('FOOD', civ.resources[ResourceTag.FOOD.code]),
                         ('IRON', civ.resources[ResourceTag.IRON.code]), ('TILES', len(civ.gathered_tiles))]
            else:
                pairs = [('ID', civ.id), ('POWER', civ.power), ('STATUS', 'DEAD')]
            label_2.set_text(', '.join('%s: %s' % (pair[0], str(pair[1])) for pair in pairs))
        else:
            label_2.set_text('')

    event_manager.add_listener(pg.MOUSEBUTTONDOWN, map_0, lambda self, tile: [show_tile_info(tile), show_civ_info(tile)])

    while not event_manager.get_quit_event():
        event_manager.update_events()
        event_manager.notify_mouse_listeners()
        surface.fill((0, 0, 0))
        pane_0.render(surface)
        pg.display.flip()
        clock.tick(24)

    pg.quit()
